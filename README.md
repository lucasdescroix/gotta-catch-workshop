**G O T T A — C A T C H**<br>
: :  Lycée Jacques Prévert<br>
: :  23-26 mars 2021<br>

————————————<br>
L'objectif de cet atelier est d'approcher le dessin de caractère à travers la conception d'une famille typographique de titrage dont chacun des membres sera inspiré par un des “éléments” ci-après : *Bug, Dragon, Electric, Fighting, Fire, Flying, Ghost, Grass, Ground, Ice, Poison, Psychic, Rock, Water*. Les proportions générales de la famille suivront celles du style *Normal*, mais le style graphique est absolument libre et ne doit répondre à aucun invariant particulier.<br>
Les inspirations donneront lieu à une collecte de mots et d'images, suivuie d'une recherche calligraphique, permettant une meilleure compréhension et conceptualisation. Les résultats pourront au choix être illustratifs ou distanciés.  


————————————<br>
**C A L E N D R I E R**<br><br>
• **Mardi 23 mars | (10h-12h, 13h-16h)**<br>
    - Lancement/introduction du sujet<br>
    - Choix des “éléments” d'inspiration<br>
    - Collecte d'images et de mots<br>
    -- Pause déjeuner<br>
    - Recherche de rythmes graphiques et calligraphiques<br>
    - Introduction au logiciel Glyphs et à la notion de composantes<br>
    <br>
• **Mercredi 24 mars | (10h-12h, 13h-16h)**<br>
    - Scan de formes graphiques produites<br>
    -- Pause déjeuner<br>
    - Travail de dessin sur Glyphs<br>
    - Suivi individuel<br>
    <br>
• **Jeudi 25 mars | (13h-18h)**<br>
    - Poursuite du travail sur Glyphs<br>
    - Suivi individuel<br>
    <br>
• **Vendredi 26 mars | (10h-12h, 13h-16h)**<br>
    - Fin du travail sur Glyphs<br>
    - Exports des fichiers typographiques<br>
    - Début de réflexion sur la mise en page de visuels ”spécimen”<br>
    -- Pause déjeuner<br>
    - Mise en page de visuels “spécimen” et choix d'images d'inspiration/recherches<br>
    - Intégration des visuels dans les pages web<br>



————————————<br>
**L I E N S**

• [Ressources typographiques diverses (rassemblées par Studio Triple)](https://gitlab.com/StudioTriple/vite_et_mal/-/blob/master/documentation/type%20design%20education%20ressources.md)<br>
• [Tutoriels pour le logiciel Glyphs](https://glyphsapp.com/learn)<br>
• [Oh No Type Co. blog](https://ohnotype.co/blog/tagged/teaching)<br>
• [Vieux spécimens typographiques numérisés (rassemblés par Emmanuel Besse)](https://www.are.na/emmanuel-besse/type-specimens-taleuy1p7xc)<br>
• [Spécimens typographiques web](https://typespecimens.xyz/specimens/all/)<br>
• [Type Review Journal](https://fontreviewjournal.com/)<br>
• [Fonts In Use](https://fontsinuse.com/)<br>
• [Stratégies italiques](http://strategiesitaliques.fr/)<br>

————————————<br>
**R É F É R E N C E S**

• [Electric](https://www.myfonts.com/fonts/typodermic/electric/?refby=fiu)<br>
• [Elektrix](https://www.emigre.com/Fonts/Elektrix)<br>
• [Gemini](https://www.flickr.com/photos/stewf/16437312062/)<br>
• [GlyphWorld](https://femme-type.com/a-typeface-of-nine-landscapes-glyph-world/)<br>
• [Grassy](https://www.fontshop.com/families/linotype-grassy)<br>
• [Kaeru Kaeru](http://velvetyne.fr/fonts/kaeru-kaeru/)<br>
• [Pilowlava](http://velvetyne.fr/fonts/pilowlava/)<br>
• [Sea Weed](https://www.flickr.com/photos/hardwig/46965181465/in/photostream/)<br>
• [Wind](https://www.typotheque.com/fonts/wind)<br>
